﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptography_lab1
{
    public class ApostKeyAprMsgRatioTable
    {
        private double[] _messages;
        private double[] _keys;
        private CryptFunction _cf;
        private MessagesAposterioryTable _apostMsgTbl;

        //k, e
        private double[,] _table;

        public ApostKeyAprMsgRatioTable(MessagesAprioryTable apt, KeyAprioryTable kat, MessagesAposterioryTable apostMsgTbl, CryptFunction cf)
        {
            _messages = apt.GetAprioryProbTable();
            _keys = kat.GetAprioryProbTable();
            _apostMsgTbl = apostMsgTbl;

            _cf = cf;

            _table = new double[apostMsgTbl.GetK(), _apostMsgTbl.GetE()];
        }

        public double KApostE(int e, int k)
        {
            return _keys[k] * _apostMsgTbl.EApostK(k, e) / _apostMsgTbl.EAprioryProbability(e);
        }

        public double[,] Build()
        {
            for (int k = 0; k < _apostMsgTbl.GetK(); ++k)
            {
                for (int e = 0; e < _apostMsgTbl.GetE(); ++e)
                {
                    var m = _cf.M(k, e);
                    if (m != -1)
                        _table[k, e] = KApostE(e, k) / _messages[m];
                    else
                        _table[k, e] = 0;
                }
            }

            return _table;
        }

        public double[,] GetTable()
        {
            return _table;
        }
    }
}
