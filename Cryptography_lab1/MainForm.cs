﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Cryptography_lab1
{
    public partial class MainForm : Form
    {
        private CryptFunction _function;
        private MessagesAprioryTable _msgApr;
        private KeyAprioryTable _keyApr;
        private MessagesAposterioryTable _msgApost;
        private MessagesDeltaTable _delta;
        private ApostKeyAprMsgRatioTable _ratio;

        private void fillArray(int[] a)
        {
            for (int i = 0; i < a.Length; ++i)
                a[i] = -1;
        }

        private void createTable()
        {
            int mSize = Convert.ToInt32(tbMSize.Text);
            int kSize = Convert.ToInt32(tbKSize.Text);
            int eSize = Convert.ToInt32(tbESize.Text);

            _function = new CryptFunction(mSize, kSize, eSize);

            var m2k1 = CryptoConverter.Parse(tbM2K1.Text);
            var m3k2 = CryptoConverter.Parse(tbM3K2.Text);

            var r = _function.GetR();

            r[0, 1]= m2k1;
            r[1, 2] = m3k2;
        }

        private void createMsgAprTable()
        {
            _msgApr = new MessagesAprioryTable(_function.GetM());
            if (rbMsgAprEq.Checked)
            {
                _msgApr.BuildEq();
            }
            else if (rbMsgAprRnd.Checked)
            {
                _msgApr.BuildRand();
            }
        }

        private void createKeyAprTable()
        {
            _keyApr = new KeyAprioryTable(_function.GetK());
            if (rbKeyAprEq.Checked)
            {
                _keyApr.BuildKeyAprTableEquip();
            }
            else if (rbKeyAprRnd.Checked)
            {
                _keyApr.BuildKeyAprTableAuto();
            }
            else if (rbKeyAprZeros.Checked)
            {
                var zeros = new int[_function.GetK()];
                var queryZeros = tbKeyAprZeros.Text.Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
                for (int i = 0; i < zeros.Length; ++i)
                {
                    zeros[i] = queryZeros.Contains(i) ? 0 : -1;
                }
                _keyApr.BuildKeyAprTableLess(zeros);
            }
        }

        private void createMsgApostTable()
        {
            _msgApost = new MessagesAposterioryTable(_function, _msgApr, _keyApr);
            _msgApost.Build();
        }

        private void createDeltaTable()
        {
            _delta = new MessagesDeltaTable(_msgApr, _msgApost);
            _delta.Build();
        }

        private void createRatioTable()
        {
            _ratio = new ApostKeyAprMsgRatioTable(_msgApr, _keyApr, _msgApost, _function);
            _ratio.Build();
        }

        private void fillTable()
        {
            clearTable(dgvCryptFunction);

            dgvCryptFunction.Columns.Add("keys", "");
            for (int m = 0; m < _function.GetM(); ++m)
            {
                var name = CryptoConverter.ToMessage(m);
                dgvCryptFunction.Columns.Add(name, name);
            }

            var data = _function.GetR();

            for (int k = 0; k < _function.GetK(); ++k)
            {
                dgvCryptFunction.Rows.Add();
                dgvCryptFunction[0, k].Value = CryptoConverter.ToKey(k);
                for (int m = 0; m < _function.GetM(); ++m)
                {
                    dgvCryptFunction[m + 1, k].Value = CryptoConverter.ToEncryption(data[k, m]);
                }
            }
        }

        private void fillMsgAprTable()
        {
            clearTable(dgvMsgApr);

            for (int m = 0; m < _function.GetM(); ++m)
            {
                var name = CryptoConverter.ToMessage(m);
                dgvMsgApr.Columns.Add(name, name);
            }

            dgvMsgApr.Rows.Add();

            var data = _msgApr.GetAprioryProbTable();

            for (int m = 0; m < _function.GetM(); ++m)
            {
                dgvMsgApr[m, 0].Value = toString(data[m]);
            }
        }

        private void fillKeyAprTable()
        {
            clearTable(dgvKeyApr);

            var data = _keyApr.GetAprioryProbTable();

            for (int k = 0; k < _function.GetK(); ++k)
            {
                var name = CryptoConverter.ToKey(k);
                dgvKeyApr.Columns.Add(name, name);
            }
            dgvKeyApr.Rows.Add();
            for (int k = 0; k < _function.GetK(); ++k)
            {
                dgvKeyApr[k, 0].Value = toString(data[k]);
            }
        }

        private void fillMsgApostTable()
        {
            clearTable(dgvMsgApost);

            dgvMsgApost.Columns.Add("Encryptions", "");
            for (int m = 0; m < _function.GetM(); ++m)
            {
                var name = CryptoConverter.ToMessage(m);
                dgvMsgApost.Columns.Add(name, name);
            }
            for (int e = 0; e < _function.GetE(); ++e)
            {
                dgvMsgApost.Rows.Add();
                dgvMsgApost[0, e].Value = CryptoConverter.ToEncryption(e);
            }

            var data = _msgApost.GetTable();

            for (int e = 0; e < _function.GetE(); ++e)
            {
                for (int m = 0; m < _function.GetM(); ++m)
                {
                    dgvMsgApost[m + 1, e].Value = toString(data[e, m]);
                }
            }
        }

        private void fillDeltaTable()
        {
            clearTable(dgvDelta);

            dgvDelta.Columns.Add("Encryptions", "");
            for (int m = 0; m < _function.GetM(); ++m)
            {
                var name = CryptoConverter.ToMessage(m);
                dgvDelta.Columns.Add(name, name);
            }
            for (int e = 0; e < _function.GetE(); ++e)
            {
                dgvDelta.Rows.Add();
                dgvDelta[0, e].Value = CryptoConverter.ToEncryption(e);
            }

            var data = _delta.GetTable();

            for (int e = 0; e < _function.GetE(); ++e)
            {
                for (int m = 0; m < _function.GetM(); ++m)
                {
                    dgvDelta[m + 1, e].Value = toString(data[e, m]);
                }
            }
        }

        private void fillRatioTable()
        {
            clearTable(dgvRatio);

            dgvRatio.Columns.Add("Keys", "");
            for (int e = 0; e < _function.GetE(); ++e)
            {
                var name = CryptoConverter.ToEncryption(e);
                dgvRatio.Columns.Add(name, name);
            }
            for (int k = 0; k < _function.GetK(); ++k)
            {
                dgvRatio.Rows.Add();
                dgvRatio[0, k].Value = CryptoConverter.ToKey(k);
            }

            var data = _ratio.GetTable();

            for (int k = 0; k < _function.GetK(); ++k)
            {
                for (int e = 0; e < _function.GetE(); ++e)
                {
                    dgvRatio[e + 1, k].Value = toString(data[k, e]);
                }
            }
        }

        private void clearTable(DataGridView table)
        {
            table.Rows.Clear();
            table.Columns.Clear();
        }

        private string toString(double val)
        {
            return val.ToString("F2");
        }

        public MainForm()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCreateFunction_Click(object sender, EventArgs e)
        {
            createTable();
            fillTable();

            createMsgAprTable();
            fillMsgAprTable();

            createKeyAprTable();
            fillKeyAprTable();

            createMsgApostTable();
            fillMsgApostTable();

            createDeltaTable();
            fillDeltaTable();

            createRatioTable();
            fillRatioTable();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //update function
            var r = _function.GetR();
            for (int k = 0; k < _function.GetK(); ++k)
            {
                for (int m = 0; m < _function.GetM(); ++m)
                {
                    var val = dgvCryptFunction[m + 1, k].Value;
                    if (val != null)
                    {
                        r[k, m] = CryptoConverter.Parse((string)val);
                    }
                    else
                    {
                        r[k, m] = 0;
                    }
                }
            }

            if (_function.IsValidR())
            {
                MessageBox.Show("Valid function", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Invalid function", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (cbUpdateMsgs.Checked)
            {
                //update apriori messages
                createMsgAprTable();
                fillMsgAprTable();
            }

            if (cbUpdateKeys.Checked)
            {
                //update apriori keys
                createKeyAprTable();
                fillKeyAprTable();
            }

            //update aposteriori messages
            createMsgApostTable();
            fillMsgApostTable();

            //update delta table
            createDeltaTable();
            fillDeltaTable();

            //update ratio table
            createRatioTable();
            fillRatioTable();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (sfdFunction.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var file = new BinaryWriter(sfdFunction.OpenFile());
                var data = _function.GetR();

                //save m, k, e
                file.Write(_function.GetM());
                file.Write(_function.GetK());
                file.Write(_function.GetE());
                //save function
                for (int k = 0; k < _function.GetK(); ++k)
                {
                    for (int m = 0; m < _function.GetM(); ++m)
                    {
                        file.Write(data[k, m]);
                    }
                }
                //save message apriori table
                var msgAprTbl = _msgApr.GetAprioryProbTable();
                for (int m = 0; m < _function.GetM(); ++m)
                {
                    file.Write(msgAprTbl[m]);
                }
                //save key apriori table
                var keyAprTbl = _keyApr.GetAprioryProbTable();
                for (int k = 0; k < _function.GetK(); ++k)
                {
                    file.Write(keyAprTbl[k]);
                }

                file.Flush();
                file.Close();
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (ofdFunction.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var file = new BinaryReader(ofdFunction.OpenFile());
                //load m, k, e
                var m = file.ReadInt32();
                var k = file.ReadInt32();
                var _e = file.ReadInt32();

                _function = new CryptFunction(m, k, _e);
                var r = _function.GetR();

                //load function
                for (int i = 0; i < k; ++i)
                {
                    for (int j = 0; j < m; ++j)
                    {
                        r[i, j] = file.ReadInt32();
                    }
                }

                //load message apriori table
                _msgApr = new MessagesAprioryTable(m);
                var msgAprTbl = _msgApr.GetAprioryProbTable();
                for (int i = 0; i < m; ++i)
                {
                    msgAprTbl[i] = file.ReadDouble();
                }

                //load key apriori table
                _keyApr = new KeyAprioryTable(k);
                var keyAprTbl = _keyApr.GetAprioryProbTable();
                for (int i = 0; i < k; ++i)
                {
                    keyAprTbl[i] = file.ReadDouble();
                }

                file.Close();

                fillTable();
                fillMsgAprTable();
                fillKeyAprTable();
                createMsgApostTable();
                fillMsgApostTable();
                createDeltaTable();
                fillDeltaTable();
                createRatioTable();
                fillRatioTable();
            }
        }
    }
}
