﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptography_lab1
{
    public class MessagesDeltaTable
    {
        private double[] _aprTable;
        private double[,] _apostTable;
        private double[,] _table;

        private int _m;
        private int _e;

        public MessagesDeltaTable(MessagesAprioryTable aprTable, MessagesAposterioryTable apostTable)
        {
            _aprTable = aprTable.GetAprioryProbTable();
            _apostTable = apostTable.GetTable();

            _m = apostTable.GetM();
            _e = apostTable.GetE();

            //_table = new double[_aprTable.Length, _apostTable.Length];
            _table = new double[_e, _m];
        }

        public double[,] Build()
        {
            for (int e = 0; e < _e; ++e)
            {
                for (int m = 0; m < _m; ++m)
                {
                    _table[e, m] = Math.Abs(_apostTable[e, m] - _aprTable[m]);
                }
            }

            return _table;
        }

        public double[,] GetTable()
        {
            return _table;
        }
    }
}
