﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptography_lab1
{
    public class KeyAprioryTable
    {
        private int _keyCount;
        private double[] _keyAprioryProb;
        private double _sumKeyAprProb;

        public KeyAprioryTable(int keyCount)
        {
            _keyAprioryProb = new double[keyCount];
            _keyCount = keyCount;
        }

        public double[] BuildKeyAprTableAuto()
        {
            _keyAprioryProb = new double[_keyCount];
            var randNum = new Random();

            for (int i = 0; i < _keyCount; ++i)
            {
                _keyAprioryProb[i] = randNum.Next(100);
            }
            var sum = _keyAprioryProb.Sum();
            for (int i = 0; i < _keyCount; ++i)
            {
                _keyAprioryProb[i] /= sum;
            }

            _sumKeyAprProb = _keyAprioryProb.Sum();
            return _keyAprioryProb;
        }

        public double[] BuildKeyAprTableEquip()
        {
            _keyAprioryProb = new double[_keyCount];
            double keyProb = 1.0 / _keyCount;
            for (int i = 0; i < _keyCount; i++)
                _keyAprioryProb[i] = keyProb;
            _sumKeyAprProb = _keyAprioryProb.Sum();
            return _keyAprioryProb;
        }

        /// <summary>
        /// Build a key apriory table with zero keys
        /// </summary>
        /// <param name="zeros">0 for zero key; other for "active" key</param>
        /// <returns></returns>
        public double[] BuildKeyAprTableLess(int[] zeros)
        {
            _keyAprioryProb = new double[_keyCount];

            int zerosCount = 0;
            for (int i = 0; i < _keyCount; ++i)
            {
                if (zeros[i] == 0)
                    ++zerosCount;
            }

            double keyProb = 1.0 / (_keyCount - zerosCount);
            for (int i = 0; i < _keyCount; ++i)
                _keyAprioryProb[i] = (zeros[i] == 0) ? 0 : keyProb;

            _sumKeyAprProb = _keyAprioryProb.Sum();
            return _keyAprioryProb;
        }

        public double[] GetAprioryProbTable()
        {
            return _keyAprioryProb;
        }

        public double GetSumAprProb()
        {
            return _sumKeyAprProb;
        }
    }
}
