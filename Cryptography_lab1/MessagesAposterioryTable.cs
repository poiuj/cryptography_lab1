﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptography_lab1
{
    public class MessagesAposterioryTable
    {
        //m, k, e
        private bool[, ,] _phi;
        private CryptFunction _cf;
        private MessagesAprioryTable _apt;
        private KeyAprioryTable _kat;

        //e, m
        private double[,] _probabilities;

        public MessagesAposterioryTable(CryptFunction cf, MessagesAprioryTable apt, KeyAprioryTable kat)
        {
            int cfm = cf.GetM();
            int cfk = cf.GetK();
            int cfe = cf.GetE();

            _phi = new bool[cfm, cfk, cfe];
            var cfr = cf.GetR();
            for (int k = 0; k < cfk; ++k)
            {
                for (int m = 0; m < cfm; ++m)
                {
                    if (cfr[k, m] != -1)
                        _phi[m, k, cfr[k, m]] = true;
                }
            }

            _apt = apt;
            _kat = kat;
            _cf = cf;

            _probabilities = new double[cfe, cfm];
        }

        public double EAprioryProbability(int e)
        {
            var result = 0.0;
            var messagesAprioryTable = _apt.GetAprioryProbTable();
            var keyAprTable = _kat.GetAprioryProbTable();
            for (int i = 0; i < _cf.GetM(); ++i)
            {
                //result += messagesAprioryTable[i] * _cf.Nmi(i, e) / _cf.GetK();
                for (int k = 0; k < _cf.GetK(); ++k)
                {
                    if (_phi[i, k, e])
                        result += keyAprTable[k] * messagesAprioryTable[i];
                }
            }

            return result;
        }

        public double EApostProbability(int m, int e)
        {
            var result = 0.0;
            var kTable = _kat.GetAprioryProbTable();
            for (int k = 0; k < kTable.Length; ++k)
            {
                if (_phi[m, k, e])
                    result += kTable[k];
            }

            return result;
        }

        public double EApostK(int k, int e)
        {
            var messages = _apt.GetAprioryProbTable();
            var result = 0.0;

            for (int m = 0; m < messages.Length; ++m)
            {
                if (_phi[m, k, e])
                    result += messages[m];
            }

            return result;
        }

        public double[,] Build()
        {
            var mTbl = _apt.GetAprioryProbTable();
            for (int e = 0; e < _cf.GetE(); ++e)
            {
                for (int m = 0; m < _cf.GetM(); ++m)
                {
                    _probabilities[e, m] = mTbl[m] * EApostProbability(m, e) / EAprioryProbability(e);
                }
            }

            return _probabilities;
        }

        public double[,] GetTable()
        {
            return _probabilities;
        }

        public int GetM()
        {
            return _cf.GetM();
        }

        public int GetE()
        {
            return _cf.GetE();
        }

        public int GetK()
        {
            return _kat.GetAprioryProbTable().Length;
        }
    }
}
