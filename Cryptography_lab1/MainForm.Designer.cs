﻿namespace Cryptography_lab1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpCryptFunction = new System.Windows.Forms.TabPage();
            this.createFunctionPanel = new System.Windows.Forms.Panel();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbM3K2 = new System.Windows.Forms.TextBox();
            this.tbM2K1 = new System.Windows.Forms.TextBox();
            this.tbESize = new System.Windows.Forms.TextBox();
            this.btnCreateFunction = new System.Windows.Forms.Button();
            this.tbKSize = new System.Windows.Forms.TextBox();
            this.tbMSize = new System.Windows.Forms.TextBox();
            this.dgvCryptFunction = new System.Windows.Forms.DataGridView();
            this.tpMsgApr = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbMsgAprRnd = new System.Windows.Forms.RadioButton();
            this.rbMsgAprEq = new System.Windows.Forms.RadioButton();
            this.dgvMsgApr = new System.Windows.Forms.DataGridView();
            this.tpKeyApr = new System.Windows.Forms.TabPage();
            this.tpMsgApost = new System.Windows.Forms.TabPage();
            this.tpDeltaTable = new System.Windows.Forms.TabPage();
            this.tpRatioTable = new System.Windows.Forms.TabPage();
            this.ofdFunction = new System.Windows.Forms.OpenFileDialog();
            this.sfdFunction = new System.Windows.Forms.SaveFileDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbKeyAprEq = new System.Windows.Forms.RadioButton();
            this.rbKeyAprRnd = new System.Windows.Forms.RadioButton();
            this.dgvKeyApr = new System.Windows.Forms.DataGridView();
            this.rbKeyAprZeros = new System.Windows.Forms.RadioButton();
            this.tbKeyAprZeros = new System.Windows.Forms.TextBox();
            this.dgvMsgApost = new System.Windows.Forms.DataGridView();
            this.dgvDelta = new System.Windows.Forms.DataGridView();
            this.dgvRatio = new System.Windows.Forms.DataGridView();
            this.cbUpdateMsgs = new System.Windows.Forms.CheckBox();
            this.cbUpdateKeys = new System.Windows.Forms.CheckBox();
            this.tcMain.SuspendLayout();
            this.tpCryptFunction.SuspendLayout();
            this.createFunctionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCryptFunction)).BeginInit();
            this.tpMsgApr.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMsgApr)).BeginInit();
            this.tpKeyApr.SuspendLayout();
            this.tpMsgApost.SuspendLayout();
            this.tpDeltaTable.SuspendLayout();
            this.tpRatioTable.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKeyApr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMsgApost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDelta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRatio)).BeginInit();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tpCryptFunction);
            this.tcMain.Controls.Add(this.tpMsgApr);
            this.tcMain.Controls.Add(this.tpKeyApr);
            this.tcMain.Controls.Add(this.tpMsgApost);
            this.tcMain.Controls.Add(this.tpDeltaTable);
            this.tcMain.Controls.Add(this.tpRatioTable);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Location = new System.Drawing.Point(0, 0);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(557, 314);
            this.tcMain.TabIndex = 2;
            // 
            // tpCryptFunction
            // 
            this.tpCryptFunction.Controls.Add(this.createFunctionPanel);
            this.tpCryptFunction.Controls.Add(this.dgvCryptFunction);
            this.tpCryptFunction.Location = new System.Drawing.Point(4, 22);
            this.tpCryptFunction.Name = "tpCryptFunction";
            this.tpCryptFunction.Padding = new System.Windows.Forms.Padding(3);
            this.tpCryptFunction.Size = new System.Drawing.Size(549, 288);
            this.tpCryptFunction.TabIndex = 0;
            this.tpCryptFunction.Text = "Cryptography Function";
            this.tpCryptFunction.UseVisualStyleBackColor = true;
            // 
            // createFunctionPanel
            // 
            this.createFunctionPanel.Controls.Add(this.cbUpdateKeys);
            this.createFunctionPanel.Controls.Add(this.cbUpdateMsgs);
            this.createFunctionPanel.Controls.Add(this.btnLoad);
            this.createFunctionPanel.Controls.Add(this.btnSave);
            this.createFunctionPanel.Controls.Add(this.btnUpdate);
            this.createFunctionPanel.Controls.Add(this.label5);
            this.createFunctionPanel.Controls.Add(this.label4);
            this.createFunctionPanel.Controls.Add(this.label3);
            this.createFunctionPanel.Controls.Add(this.label2);
            this.createFunctionPanel.Controls.Add(this.label1);
            this.createFunctionPanel.Controls.Add(this.tbM3K2);
            this.createFunctionPanel.Controls.Add(this.tbM2K1);
            this.createFunctionPanel.Controls.Add(this.tbESize);
            this.createFunctionPanel.Controls.Add(this.btnCreateFunction);
            this.createFunctionPanel.Controls.Add(this.tbKSize);
            this.createFunctionPanel.Controls.Add(this.tbMSize);
            this.createFunctionPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.createFunctionPanel.Location = new System.Drawing.Point(3, 187);
            this.createFunctionPanel.Name = "createFunctionPanel";
            this.createFunctionPanel.Size = new System.Drawing.Size(543, 98);
            this.createFunctionPanel.TabIndex = 5;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(463, 17);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 14;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(382, 17);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(301, 56);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 12;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(111, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "f(m3,k2):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "f(m2,k1):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(217, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "E:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(111, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "K:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "M:";
            // 
            // tbM3K2
            // 
            this.tbM3K2.Location = new System.Drawing.Point(114, 58);
            this.tbM3K2.Name = "tbM3K2";
            this.tbM3K2.Size = new System.Drawing.Size(100, 20);
            this.tbM3K2.TabIndex = 6;
            // 
            // tbM2K1
            // 
            this.tbM2K1.Location = new System.Drawing.Point(8, 58);
            this.tbM2K1.Name = "tbM2K1";
            this.tbM2K1.Size = new System.Drawing.Size(100, 20);
            this.tbM2K1.TabIndex = 5;
            // 
            // tbESize
            // 
            this.tbESize.Location = new System.Drawing.Point(220, 19);
            this.tbESize.Name = "tbESize";
            this.tbESize.Size = new System.Drawing.Size(100, 20);
            this.tbESize.TabIndex = 4;
            // 
            // btnCreateFunction
            // 
            this.btnCreateFunction.Location = new System.Drawing.Point(220, 55);
            this.btnCreateFunction.Name = "btnCreateFunction";
            this.btnCreateFunction.Size = new System.Drawing.Size(75, 23);
            this.btnCreateFunction.TabIndex = 3;
            this.btnCreateFunction.Text = "Create";
            this.btnCreateFunction.UseVisualStyleBackColor = true;
            this.btnCreateFunction.Click += new System.EventHandler(this.btnCreateFunction_Click);
            // 
            // tbKSize
            // 
            this.tbKSize.Location = new System.Drawing.Point(114, 19);
            this.tbKSize.Name = "tbKSize";
            this.tbKSize.Size = new System.Drawing.Size(100, 20);
            this.tbKSize.TabIndex = 1;
            // 
            // tbMSize
            // 
            this.tbMSize.Location = new System.Drawing.Point(8, 19);
            this.tbMSize.Name = "tbMSize";
            this.tbMSize.Size = new System.Drawing.Size(100, 20);
            this.tbMSize.TabIndex = 0;
            // 
            // dgvCryptFunction
            // 
            this.dgvCryptFunction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCryptFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCryptFunction.Location = new System.Drawing.Point(3, 3);
            this.dgvCryptFunction.Name = "dgvCryptFunction";
            this.dgvCryptFunction.Size = new System.Drawing.Size(543, 282);
            this.dgvCryptFunction.TabIndex = 4;
            // 
            // tpMsgApr
            // 
            this.tpMsgApr.Controls.Add(this.panel1);
            this.tpMsgApr.Controls.Add(this.dgvMsgApr);
            this.tpMsgApr.Location = new System.Drawing.Point(4, 22);
            this.tpMsgApr.Name = "tpMsgApr";
            this.tpMsgApr.Padding = new System.Windows.Forms.Padding(3);
            this.tpMsgApr.Size = new System.Drawing.Size(549, 298);
            this.tpMsgApr.TabIndex = 1;
            this.tpMsgApr.Text = "Apriori Messages";
            this.tpMsgApr.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbMsgAprRnd);
            this.panel1.Controls.Add(this.rbMsgAprEq);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 224);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(543, 71);
            this.panel1.TabIndex = 2;
            // 
            // rbMsgAprRnd
            // 
            this.rbMsgAprRnd.AutoSize = true;
            this.rbMsgAprRnd.Checked = true;
            this.rbMsgAprRnd.Location = new System.Drawing.Point(5, 26);
            this.rbMsgAprRnd.Name = "rbMsgAprRnd";
            this.rbMsgAprRnd.Size = new System.Drawing.Size(65, 17);
            this.rbMsgAprRnd.TabIndex = 1;
            this.rbMsgAprRnd.TabStop = true;
            this.rbMsgAprRnd.Text = "Random";
            this.rbMsgAprRnd.UseVisualStyleBackColor = true;
            // 
            // rbMsgAprEq
            // 
            this.rbMsgAprEq.AutoSize = true;
            this.rbMsgAprEq.Location = new System.Drawing.Point(5, 3);
            this.rbMsgAprEq.Name = "rbMsgAprEq";
            this.rbMsgAprEq.Size = new System.Drawing.Size(52, 17);
            this.rbMsgAprEq.TabIndex = 0;
            this.rbMsgAprEq.Text = "Equal";
            this.rbMsgAprEq.UseVisualStyleBackColor = true;
            // 
            // dgvMsgApr
            // 
            this.dgvMsgApr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMsgApr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMsgApr.Location = new System.Drawing.Point(3, 3);
            this.dgvMsgApr.Name = "dgvMsgApr";
            this.dgvMsgApr.Size = new System.Drawing.Size(543, 292);
            this.dgvMsgApr.TabIndex = 0;
            // 
            // tpKeyApr
            // 
            this.tpKeyApr.Controls.Add(this.dgvKeyApr);
            this.tpKeyApr.Controls.Add(this.panel2);
            this.tpKeyApr.Location = new System.Drawing.Point(4, 22);
            this.tpKeyApr.Name = "tpKeyApr";
            this.tpKeyApr.Padding = new System.Windows.Forms.Padding(3);
            this.tpKeyApr.Size = new System.Drawing.Size(549, 298);
            this.tpKeyApr.TabIndex = 2;
            this.tpKeyApr.Text = "Apriory Keys";
            this.tpKeyApr.UseVisualStyleBackColor = true;
            // 
            // tpMsgApost
            // 
            this.tpMsgApost.Controls.Add(this.dgvMsgApost);
            this.tpMsgApost.Location = new System.Drawing.Point(4, 22);
            this.tpMsgApost.Name = "tpMsgApost";
            this.tpMsgApost.Padding = new System.Windows.Forms.Padding(3);
            this.tpMsgApost.Size = new System.Drawing.Size(549, 298);
            this.tpMsgApost.TabIndex = 3;
            this.tpMsgApost.Text = "Aposteriori Messages";
            this.tpMsgApost.UseVisualStyleBackColor = true;
            // 
            // tpDeltaTable
            // 
            this.tpDeltaTable.Controls.Add(this.dgvDelta);
            this.tpDeltaTable.Location = new System.Drawing.Point(4, 22);
            this.tpDeltaTable.Name = "tpDeltaTable";
            this.tpDeltaTable.Padding = new System.Windows.Forms.Padding(3);
            this.tpDeltaTable.Size = new System.Drawing.Size(549, 298);
            this.tpDeltaTable.TabIndex = 4;
            this.tpDeltaTable.Text = "Delta Table";
            this.tpDeltaTable.UseVisualStyleBackColor = true;
            // 
            // tpRatioTable
            // 
            this.tpRatioTable.Controls.Add(this.dgvRatio);
            this.tpRatioTable.Location = new System.Drawing.Point(4, 22);
            this.tpRatioTable.Name = "tpRatioTable";
            this.tpRatioTable.Padding = new System.Windows.Forms.Padding(3);
            this.tpRatioTable.Size = new System.Drawing.Size(549, 298);
            this.tpRatioTable.TabIndex = 5;
            this.tpRatioTable.Text = "Ratio Table";
            this.tpRatioTable.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbKeyAprZeros);
            this.panel2.Controls.Add(this.rbKeyAprZeros);
            this.panel2.Controls.Add(this.rbKeyAprRnd);
            this.panel2.Controls.Add(this.rbKeyAprEq);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 224);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(543, 71);
            this.panel2.TabIndex = 2;
            // 
            // rbKeyAprEq
            // 
            this.rbKeyAprEq.AutoSize = true;
            this.rbKeyAprEq.Checked = true;
            this.rbKeyAprEq.Location = new System.Drawing.Point(3, 3);
            this.rbKeyAprEq.Name = "rbKeyAprEq";
            this.rbKeyAprEq.Size = new System.Drawing.Size(52, 17);
            this.rbKeyAprEq.TabIndex = 1;
            this.rbKeyAprEq.TabStop = true;
            this.rbKeyAprEq.Text = "Equal";
            this.rbKeyAprEq.UseVisualStyleBackColor = true;
            // 
            // rbKeyAprRnd
            // 
            this.rbKeyAprRnd.AutoSize = true;
            this.rbKeyAprRnd.Location = new System.Drawing.Point(3, 26);
            this.rbKeyAprRnd.Name = "rbKeyAprRnd";
            this.rbKeyAprRnd.Size = new System.Drawing.Size(65, 17);
            this.rbKeyAprRnd.TabIndex = 2;
            this.rbKeyAprRnd.Text = "Random";
            this.rbKeyAprRnd.UseVisualStyleBackColor = true;
            // 
            // dgvKeyApr
            // 
            this.dgvKeyApr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKeyApr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvKeyApr.Location = new System.Drawing.Point(3, 3);
            this.dgvKeyApr.Name = "dgvKeyApr";
            this.dgvKeyApr.Size = new System.Drawing.Size(543, 221);
            this.dgvKeyApr.TabIndex = 3;
            // 
            // rbKeyAprZeros
            // 
            this.rbKeyAprZeros.AutoSize = true;
            this.rbKeyAprZeros.Location = new System.Drawing.Point(5, 49);
            this.rbKeyAprZeros.Name = "rbKeyAprZeros";
            this.rbKeyAprZeros.Size = new System.Drawing.Size(55, 17);
            this.rbKeyAprZeros.TabIndex = 3;
            this.rbKeyAprZeros.TabStop = true;
            this.rbKeyAprZeros.Text = "Zeros:";
            this.rbKeyAprZeros.UseVisualStyleBackColor = true;
            // 
            // tbKeyAprZeros
            // 
            this.tbKeyAprZeros.Location = new System.Drawing.Point(66, 49);
            this.tbKeyAprZeros.Name = "tbKeyAprZeros";
            this.tbKeyAprZeros.Size = new System.Drawing.Size(100, 20);
            this.tbKeyAprZeros.TabIndex = 4;
            // 
            // dgvMsgApost
            // 
            this.dgvMsgApost.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMsgApost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMsgApost.Location = new System.Drawing.Point(3, 3);
            this.dgvMsgApost.Name = "dgvMsgApost";
            this.dgvMsgApost.Size = new System.Drawing.Size(543, 292);
            this.dgvMsgApost.TabIndex = 0;
            // 
            // dgvDelta
            // 
            this.dgvDelta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDelta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDelta.Location = new System.Drawing.Point(3, 3);
            this.dgvDelta.Name = "dgvDelta";
            this.dgvDelta.Size = new System.Drawing.Size(543, 292);
            this.dgvDelta.TabIndex = 0;
            // 
            // dgvRatio
            // 
            this.dgvRatio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRatio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRatio.Location = new System.Drawing.Point(3, 3);
            this.dgvRatio.Name = "dgvRatio";
            this.dgvRatio.Size = new System.Drawing.Size(543, 292);
            this.dgvRatio.TabIndex = 0;
            // 
            // cbUpdateMsgs
            // 
            this.cbUpdateMsgs.AutoSize = true;
            this.cbUpdateMsgs.Location = new System.Drawing.Point(382, 55);
            this.cbUpdateMsgs.Name = "cbUpdateMsgs";
            this.cbUpdateMsgs.Size = new System.Drawing.Size(112, 17);
            this.cbUpdateMsgs.TabIndex = 15;
            this.cbUpdateMsgs.Text = "Update Messages";
            this.cbUpdateMsgs.UseVisualStyleBackColor = true;
            // 
            // cbUpdateKeys
            // 
            this.cbUpdateKeys.AutoSize = true;
            this.cbUpdateKeys.Location = new System.Drawing.Point(382, 76);
            this.cbUpdateKeys.Name = "cbUpdateKeys";
            this.cbUpdateKeys.Size = new System.Drawing.Size(87, 17);
            this.cbUpdateKeys.TabIndex = 16;
            this.cbUpdateKeys.Text = "Update Keys";
            this.cbUpdateKeys.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 314);
            this.Controls.Add(this.tcMain);
            this.Name = "MainForm";
            this.Text = "Lab1";
            this.tcMain.ResumeLayout(false);
            this.tpCryptFunction.ResumeLayout(false);
            this.createFunctionPanel.ResumeLayout(false);
            this.createFunctionPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCryptFunction)).EndInit();
            this.tpMsgApr.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMsgApr)).EndInit();
            this.tpKeyApr.ResumeLayout(false);
            this.tpMsgApost.ResumeLayout(false);
            this.tpDeltaTable.ResumeLayout(false);
            this.tpRatioTable.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKeyApr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMsgApost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDelta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRatio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpCryptFunction;
        private System.Windows.Forms.TabPage tpMsgApr;
        private System.Windows.Forms.Panel createFunctionPanel;
        private System.Windows.Forms.TextBox tbM3K2;
        private System.Windows.Forms.TextBox tbM2K1;
        private System.Windows.Forms.TextBox tbESize;
        private System.Windows.Forms.Button btnCreateFunction;
        private System.Windows.Forms.TextBox tbKSize;
        private System.Windows.Forms.TextBox tbMSize;
        private System.Windows.Forms.DataGridView dgvCryptFunction;
        private System.Windows.Forms.TabPage tpKeyApr;
        private System.Windows.Forms.TabPage tpMsgApost;
        private System.Windows.Forms.TabPage tpDeltaTable;
        private System.Windows.Forms.TabPage tpRatioTable;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.OpenFileDialog ofdFunction;
        private System.Windows.Forms.SaveFileDialog sfdFunction;
        private System.Windows.Forms.DataGridView dgvMsgApr;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbMsgAprRnd;
        private System.Windows.Forms.RadioButton rbMsgAprEq;
        private System.Windows.Forms.DataGridView dgvKeyApr;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbKeyAprRnd;
        private System.Windows.Forms.RadioButton rbKeyAprEq;
        private System.Windows.Forms.TextBox tbKeyAprZeros;
        private System.Windows.Forms.RadioButton rbKeyAprZeros;
        private System.Windows.Forms.DataGridView dgvMsgApost;
        private System.Windows.Forms.DataGridView dgvDelta;
        private System.Windows.Forms.DataGridView dgvRatio;
        private System.Windows.Forms.CheckBox cbUpdateKeys;
        private System.Windows.Forms.CheckBox cbUpdateMsgs;

    }
}

