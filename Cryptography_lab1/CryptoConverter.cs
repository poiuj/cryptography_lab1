﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptography_lab1
{
    public static class CryptoConverter
    {
        public static int Parse(string obj)
        {
            return Convert.ToInt32(obj.Substring(1)) - 1;
        }

        private static string Encode(string type, int val)
        {
            return type + (val + 1).ToString();
        }

        public static string ToMessage(int msg)
        {
            return Encode("m", msg);
        }

        public static string ToKey(int key)
        {
            return Encode("k", key);
        }

        public static string ToEncryption(int e)
        {
            return Encode("e", e);
        }
    }
}
