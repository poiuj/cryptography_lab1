﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptography_lab1
{
    public class Key
    {
        private int _value;
        private double _aprioryProbability;
        private double _aposterioryProbability;

        public Key(int value, double aprProb, double apostProb)
        {
            _value = value;
            _aprioryProbability = aprProb;
            _aposterioryProbability = apostProb;
        }

        public int GetValue()
        {
            return _value;
        }

        public double AprioryProbability()
        {
            return _aprioryProbability;
        }

        public double AposterioryProbability()
        {
            return _aposterioryProbability;
        }
    }
}
