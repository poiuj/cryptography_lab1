﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptography_lab1
{
    public class MessagesAprioryTable
    {
        private double[] _aprioryProbabilities;
        private double _sumAprProbability;
        private int _m;

        public MessagesAprioryTable(int messagesCount)
        {
            _aprioryProbabilities = new double[messagesCount];
            _m = messagesCount;
        }

        public double[] BuildRand()
        {
            var randNum = new Random();

            for (int i = 0; i < _m; ++i)
            {
                _aprioryProbabilities[i] = randNum.Next(100);
            }
            var sum = _aprioryProbabilities.Sum();
            for (int i = 0; i < _m; ++i)
            {
                _aprioryProbabilities[i] /= sum;
            }
            _sumAprProbability = _aprioryProbabilities.Sum();

            return _aprioryProbabilities;
        }

        public double[] BuildEq()
        {
            var p = 1.0 / _m;
            for (int i = 0; i < _m; ++i)
            {
                _aprioryProbabilities[i] = p;
            }
            _sumAprProbability = _aprioryProbabilities.Sum();

            return _aprioryProbabilities;
        }

        public double[] GetAprioryProbTable()
        {
            return _aprioryProbabilities;
        }

        public double GetSumAprProb()
        {
            return _sumAprProbability;
        }
    }
}
