﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptography_lab1
{
    public class CryptFunction
    {
        private int _m;
        private int _k;
        private int _e;


        //[keys, messages]
        private int[,] _r;

        /*private int _m2k1;
        private int _m3k2;*/

        private int[][] _settedRows;

        private Random _rand = new Random();

        public CryptFunction(int m, int k, int e)
        {
            _m = m;
            _k = k;
            _e = e;

            _r = new int[k, m];
            _settedRows = new int[k][];

            InitR();
        }

        public void InitR()
        {
            for (int i = 0; i < _k; ++i)
            {
                for (int j = 0; j < _m; ++j)
                {
                    _r[i, j] = -1;
                }
            }
        }
        
        private bool rowsEqual(int[] a, int k)
        {
            for (int i = 0; i < a.Length; ++i)
            {
                if (a[i] != _r[k, i])
                    return false;
            }

            return true;
        }

        private bool findRow(int[] row)
        {
            for (int i = 0; i < _k; ++i)
            {
                if (rowsEqual(row, i))
                    return true;
            }

            return false;
        }

        private int[] copyRow(int k)
        {
            var result = new int[_m];
            for (int i = 0; i < _m; ++i)
            {
                result[i] = _r[k, i];
            }

            return result;
        }

        public void SetRow(int[] a, int k)
        {
            _settedRows[k] = a;
        }

        private int getEInColumn(int e, int m)
        {
            var sum = 0;
            for (int k = 0; k < _k; ++k)
            {
                if (_r[k, m] == e)
                    ++sum;
            }

            return sum;
        }

        private int getEInRow(int e, int k)
        {
            var sum = 0;
            for (int m = 0; m < _m; ++m)
            {
                if (_r[k, m] == e)
                    ++sum;
            }

            return sum;
        }

        public bool IsValidR()
        {
            for (int e = 0; e < _e; ++e)
            {
                for (int k = 0; k < _k; ++k)
                {
                    if (getEInRow(e, k) != 1)
                        return false;
                }

                var currCount = getEInColumn(e, 0);
                for (int m = 0; m < _m; ++m)
                {
                    if (getEInColumn(e, m) != currCount)
                        return false;
                }
            }

            return true;
        }

        private void fillRowRandomly(int k)
        {
            int[] temp;
            do
            {
                if (_settedRows[k] == null)
                {
                    temp = copyRow(k);
                }
                else
                {
                    temp = new int[_m];
                    _settedRows[k].CopyTo(temp, 0);
                }

                int excludeE = -1;
                for (int i = 0; i < _m; ++i)
                    if (temp[i] != -1)
                        excludeE = temp[i];

                for (int i = 0; i < _m; ++i)
                {
                    if (i == excludeE)
                        continue;

                    int index;
                    do
                    {
                        index = _rand.Next(_m);
                    } while (temp[index] != -1);
                    temp[index] = i;
                }

            } while (findRow(temp));

            for (int m = 0; m < _m; ++m)
            {
                _r[k, m] = temp[m];
            }
        }

        public void FillRRandomly()
        {
            do
            {
                InitR();
                for (int i = 0; i < _k; ++i)
                {
                    fillRowRandomly(i);
                }
            } while (!IsValidR());
        }

        public int[,] GetR()
        {
            return _r;
        }

        public int GetM()
        {
            return _m;
        }

        public int GetK()
        {
            return _k;
        }

        public int GetE()
        {
            return _e;
        }

        public int Nmi(int m, int e)
        {
            int kCount = 0;
            for (int k = 0; k < _k; ++k)
            {
                if (_r[k, m] == e)
                    ++kCount;
            }

            return kCount;
        }

        public int M(int k, int e)
        {
            for (int m = 0; m < _m; ++m)
            {
                if (_r[k, m] == e)
                    return m;
            }

            return -1;
        }
    }
}